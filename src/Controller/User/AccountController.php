<?php

namespace App\Controller\User;

use App\Entity\Fiche;
use App\Entity\Patient;
use App\Repository\FicheRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class AccountController extends AbstractController
{
    /**
     * @Route("/user/account", name="user_account")
     * @IsGranted("ROLE_USER")
     */
    public function index()
    {
        $user=$this->getUser();
        return $this->render('user/account/index.html.twig', [
            'user'=>$user
        ]);
    }

    /**
     * Function history
     * @Route("/auxil/user/account/history",name="user_history")
     * @IsGranted("ROLE_USER")
     * Mon compte user auxiliaire
     * User: emayemba
     * Date: 25/10/2020
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function history(PaginatorInterface $paginator,FicheRepository $repository,Request $request,EntityManagerInterface $manager,NormalizerInterface $normalizer){
        $user=$this->getUser();

        $pagination=$paginator->paginate( $repository->fichesUser($user), /* query NOT result */
           $request->query->getInt('page', 1), /*page number*/
          5 /*limit per page*/);

        return $this->render('user/account/history_auxi.html.twig',[
            'user'=>$this->getUser(),
            'pagination'=>$pagination
        ]);
    }
}
