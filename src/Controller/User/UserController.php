<?php

namespace App\Controller\User;

use App\Entity\User;
use App\Form\User\UserPasswordLostNewType;
use App\Form\User\UserPasswordLostType;
use App\Form\User\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UserController extends AbstractController
{
    /**
     * @Route("/user/register", name="user_register")
     */
    public function index(UserRepository $userRepository,Request $request,
                          UserPasswordEncoderInterface $encoder,EntityManagerInterface $manager,MailerInterface $mailer)
    {
        $user =new User();
        $form=$this->createForm(UserType::class,$user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $pass=$user->getPassword();
            $password=$encoder->encodePassword($user,$pass);
            $user->setPassword($password);
            $manager->persist($user);
            $manager->flush();

            //envoi d'un mail à la direction pour signifier de l'inscription d'un nouveau user
            $email=(new TemplatedEmail())
                ->from('noreply@tulipa.fr')
                ->to('abinfirmiere@gmail.com ')
                ->addTo('s.safa@adseniorslyon.com')
                ->priority(Email::PRIORITY_HIGHEST)
                ->htmlTemplate('emails/inscription_alert.html.twig')
                ->context([
                    'user' => $user
                ]);
            $mailer->send($email);
            return $this->render('user/user/waiting_validation.html.twig');
        }
        return $this->render('user/user/new.html.twig', [
        'form'=>$form->createView()
        ]);
    }

    /**
     * Function login
     * @Route("/login",name="login")
     * User: emayemba
     * Date: 22/10/2020
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function login(AuthenticationUtils $utils){
        return $this->render('user/user/login.html.twig',[
            'error'=>$utils->getLastAuthenticationError(),
            'lastUserName'=>$utils->getLastUsername()
        ]);
    }


    /**
     * Function logout
     * @Route("/user/logout",name="logout")
     * User: emayemba
     * Date: 22/10/2020
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logout(){
        return $this->redirectToRoute('login');
    }

    /**
     * Function passwodlost
     * @Route("/user/lost/password",name="user_lostpassword")
     * User: emayemba
     * Date: 23/10/2020
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function passwordlost(Request $request,UserRepository $userRepository,TokenGeneratorInterface $tokenGenerator,
                                MailerInterface $mailer,EntityManagerInterface $manager)
    {

        $form=$this->createForm(UserPasswordLostType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
           $emailRef=$form->get('email')->getData();
           $user=$userRepository->findOneBy(['email'=>$emailRef]);

           if(!$user){
               $this->addFlash('success', 'Cette adresse e-mail est inconnue');
               // On retourne sur la page de connexion
               return $this->redirectToRoute('login');

           }else{

            $token=$tokenGenerator->generateToken();

               try{
                   $user->setResetToken($token);
                   $manager->persist($user);
                   $manager->flush();
               } catch (\Exception $e) {
                   $this->addFlash('warning', $e->getMessage());
                   return $this->redirectToRoute('app_login');
               }

            $email=(new TemplatedEmail())
                ->from('noreply@tulipa.fr')
                ->to($user->getEmail())
                ->subject('Réinitialisation mot de passe ')
                ->priority(Email::PRIORITY_HIGHEST)

                // path of the Twig template to render
                ->htmlTemplate('emails/userlostpassword.html.twig')

                // pass variables (name => value) to the template
                ->context([
                    'token' => $token,
                ]);
            $mailer->send($email);
            return $this->render('user/user/waiting_confirmation_passlost.html.twig');
           }
        }

        return $this->render('user/user/lostpassword.html.twig',[
            'form'=>$form->createView()
        ]);

    }

    /**
     * @param UserRepository $userRepository
     * @Route("user/restePassword/{token}" ,name="user_resetPassword")
     * Function resetPass
     * User: emayemba
     * Date: 23/10/2020
     */
    public function resetPass(UserRepository $userRepository,$token,UserPasswordEncoderInterface $encoder,
                              Request $request,EntityManagerInterface $manager)
    {
       $form=$this->createForm(UserPasswordLostNewType::class);
       $form->handleRequest($request);
       $user=$userRepository->findOneBy(['resetToken'=>$token]);

        if(!$user){
       $this->addFlash('success', 'Votre lien n\'est plus valide');
       return $this->redirectToRoute('login');
        }else{
           if($form->isSubmitted() && $form->isValid()){
               $user->setResetToken(null);
               $pass=$form->get('password')->getData();
               $newPass=$encoder->encodePassword($user,$pass);
               $user->setPassword($newPass);
               $manager->persist($user);
               $manager->flush();
               $this->addFlash('success','Votre  mot de passe a été modifié');
               return $this->redirectToRoute('login');
           }
        }
        return $this->render('user/user/form_reset_password.html.twig',[
            'form'=>$form->createView(),
            'token'=>$token
        ]);
    }


}
