<?php

namespace App\Controller\Service;

use App\Entity\Patient;
use App\Form\SearchType;
use App\Repository\PatientRepository;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ServicePatientController extends AbstractController
{
    /**
     * @Route("service/patient", name="service_patient_index")
     * liste de tous les patients
     * @IsGranted("ROLE_SERVICE")
     */
    public function index(PatientRepository $patientRepository,PaginatorInterface $paginator,Request $request)
    {
        $form=$this->createForm(SearchType::class);
        $pagination=$paginator->paginate( $patientRepository->findAllPaginator(), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/);

        return $this->render('service/patient/index.html.twig', [
            // 'patients'=>$patientRepository->findAll(),
            'patients'=>$pagination,
            'form'=>$form->createView()

        ]);
    }

    /**
     * @param Request $request
     * @param PatientRepository $patientRepository
     * @Route("service/patient/search",name="service_patient_search")
     * @IsGranted("ROLE_SERVICE")
     * Permet la recherche de patients
     * Function searchPatient
     * User: emayemba
     * Date: 24/10/2020
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchPatient(Request $request,PatientRepository $patientRepository,PaginatorInterface $paginator){

        $form=$this->createForm(SearchType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $name=$form->get('lastName')->getData();
            $patientsRepo=$patientRepository->findPatientsByName($name);
            if(empty($patientsRepo->getResult())){
                $this->addFlash('erreur', 'Aucun article contenant ce mot clé dans le titre n\'a été trouvé, essayez en un autre.');
                return $this->render('service/patient/nopatients.html.twig');
            }else{
                $pagination=$paginator->paginate( $patientsRepo, /* query NOT result */
                    $request->query->getInt('page', 1), /*page number*/
                    10 /*limit per page*/);
                return $this->render('service/patient/index.html.twig',[
                    'patients'=>$pagination,
                    'form'=>$form->createView()
                ]);
            }

        }
        return $this->render('service/patient/index.html.twig', [
            'form'=>$form->createView(),

        ]);
    }


    /**
     * @Route("service/patient/transmissions/filtres/{id}", name="service_patient_transmissions")
     * @IsGranted("ROLE_SERVICE")
     */
    public function transmis(Patient $patient)
    {
        return $this->render('service/patient/transmissions.html.twig', [
            'patient'=>$patient
        ]);
    }

}
