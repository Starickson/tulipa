<?php

namespace App\Controller\Service;

use App\Entity\Fiche;
use App\Entity\Patient;
use App\Repository\FicheRepository;
use Dompdf\Dompdf;
use Dompdf\Options;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Snappy\Pdf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ServiceFicheController extends AbstractController
{

    /**
     * @Route("/service/fiche", name="service_fiche_index")
     * @IsGranted("ROLE_SERVICE")
     */
    public function indexDate(FicheRepository $ficheRepository ,Request $request,PaginatorInterface $paginator)
    {
        $pagination = $paginator->paginate(
            $ficheRepository->findAllFichesPaginator(), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        return $this->render('service/fiche/index.html.twig', [
            'fiches'=>$pagination
        ]);
    }

    /**
     * @Route("service/fiche/transmissions/list/{id}", name="service_fiche_transmissions")
     * @IsGranted("ROLE_SERVICE")
     */
    public function transmis(Patient $patient,PaginatorInterface  $paginator,FicheRepository  $ficheRepository,Request $request)
    {
        $pagination = $paginator->paginate(
            $ficheRepository->fichiersPatientPaginator($patient), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        return $this->render('service/fiche/transmissions.html.twig', [
            'fiches'=>$pagination,
            'patient'=>$patient
        ]);

    }


    /**
     * Function patienFicheFiler
     * @Route("/service/fiches/patient/{id}",name="service_patientFiche_filter")
     * @IsGranted("ROLE_SERVICE")
     * User: emayemba
     * Date: 26/10/2020
     * Télécharger en pdf les fiches d'un patient sur une période donnée avec Dompdf
     * @return Response
     */
    public function patienFicheFiler(Patient $patient ,FicheRepository $ficheRepository,Request $request,Pdf $pdf){

        $date1=$request->get('_date1');
        $date2=$request->get('_date2');
        //Si la date de fin n'est pas définie alors je la fixe à today
        if(!$date2) {
            $date2 = date('Y-m-d');
        }
        $fiches=$ficheRepository->fichePatienfilter($patient,$date1,$date2);

        // on définit les options
        $options = new Options();
        //police par défaut
        $options->set('defaultFont', 'Courier');
        $options->setIsRemoteEnabled(true);

        //On instancie dompdf
        $dompdf = new Dompdf($options);

        //on génère le html
        $html=$this->renderView('service/fiche/transmissionsFiltre.html.twig', [
            'fiches'=>$fiches,
            'patient'=>$patient
        ]);

        $dompdf->loadHtml($html);

        $dompdf->setPaper('A4','portrait');

        // (Optional) Setup the paper size and orientation
        //     $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as pdf
        $dompdf->render();

        // on génère un fichier
        $fichier='Periode'.'pdf';


        // Output the generated pdf to Browser ,on envoie le pdf au navigateur ,mais attention méthode de stream à définir

        $dompdf->stream($fichier,[
            'attachment'=>true
        ]);
        //La response
        return new Response();

    }


    /**
     * @param Patient $patient
     * Function show
     * @Route("/service/fiche/show/{id}", name="service_fiche_show", methods={"GET"})
     * @IsGranted("ROLE_SERVICE")
     * User: emayemba
     * Date: 28/10/2020
     * @return Response
     */
    public function show(Fiche $fiche): Response
    {
        return $this->render('service/fiche/show.html.twig', [
            'fiche' => $fiche,
        ]);
    }

}
