<?php

namespace App\Controller\Service;

use App\Entity\Patient;
use App\Repository\FicheRepository;
use App\Repository\InterventionRepository;
use App\Repository\PatientRepository;
use Dompdf\Dompdf;
use Dompdf\Options;
use Knp\Snappy\Pdf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ServiceInterventionController extends AbstractController
{
    /**
     * @Route("/service/intervention", name="service_intervention")
     * liste de toutes les intervention sur tous  les patients
     */
    public function index(InterventionRepository $interventionRepository)
    {
        return $this->render('service/intervention/index.html.twig', [
            'interventions' => $interventionRepository->findAll()
        ]);
    }

    /**
     * @param PatientRepository $patientRepository
     * @Route("/service/intervention/patient/{id}",name="service_intervention_recap")
     * @IsGranted("ROLE_SERVICE")
     * Function recapIntervention
     * Affiche la liste des intervention pour ce patient id
     * User: emayemba
     * Date: 04/11/2020
     * function récapitulatif pour récupérer l'ensemble des intervention d'un patient
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function recapIntervention(InterventionRepository $interventionRepository ,Patient $patient){
        //autre solution parameter converter
        $interventions=$interventionRepository->findByPatient($patient);
        return $this->render('service/intervention/recapIntervention.html.twig',[
            'patient'=>$patient,
            'interventions'=>$interventions
        ]);
    }

    /**
     * @Route("/service/intervention/patient/pdf/{id}",name="service_intervention_patient_pdf")
     * @IsGranted("ROLE_SERVICE")
     * Permet de générer un pdf des intervention du patient  au format voulu.Ressemble aux fiches du patient en pdf
     * User: emayemba
     * Télécharger en pdf les intervention d'un patient sur une période donnée avec Dompdf
     * @return Response
     */
    public function patienInterventionFilter(Patient $patient ,InterventionRepository $interventionRepository,Request $request){

        $date1=$request->get('_date1');
        $date2=$request->get('_date2');
        $interventions=$interventionRepository->interventionPatienfilter($patient,$date1,$date2);

        // on définit les options
        $options = new Options();
        //police par défaut
        $options->set('defaultFont', 'Courier');
        $options->setIsRemoteEnabled(true);

        //On instancie dompdf
        $dompdf = new Dompdf($options);

        //on génère le html
        $html=$this->renderView('service/intervention/diagrammeIntervention.html.twig', [
            'interventions'=>$interventions,
            'patient'=>$patient,
            'user'=>$this->getUser()
        ]);

        $dompdf->loadHtml($html);

    /*    "a3" => array(0,0,841.89,1190.55),
        "a4" => array(0,0,595.28,841.89),*/

        $customPaper = array(0,0,841.89,1190.55);
        $dompdf->setPaper($customPaper);
       // $dompdf->setPaper('A3','landscape');

        // (Optional) Setup the paper size and orientation
        //     $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as pdf
        $dompdf->render();

        // on génère un fichier
        $fichier='DIAGRAMME DE SOIN'.'pdf';

        // Output the generated pdf to Browser ,on envoie le pdf au navigateur ,mais attention méthode de stream à définir

        $dompdf->stream($fichier,[
            'attachment'=>true
        ]);
        //La response
        return new Response();

    }






}
