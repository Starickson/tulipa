<?php

namespace App\Controller\Service;

use App\Repository\FicheRepository;
use App\Repository\InterventionRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ServiceController extends AbstractController
{
    /**
     * @Route("/service/home", name="service_home")
     * @IsGranted("ROLE_SERVICE")
     */
    public function index(FicheRepository $ficheRepository,InterventionRepository $interventionRepository)
    {
        return $this->render('service/service_home.html.twig', [
            'fiches'=>$ficheRepository->findAllLastFiche(),
            'interventions'=>$interventionRepository->findAllLastInterventions()

        ]);
    }
}
