<?php

namespace App\Controller\Admin;

use App\Entity\Family;
use App\Entity\Patient;
use App\Entity\User;
use App\Form\Patient\PatientEditType;
use App\Form\Patient\PatientType;
use App\Repository\FicheRepository;
use App\Repository\PatientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Dompdf\Dompdf;
use Dompdf\Options;

class AdminPatientController extends AbstractController
{
    /**
     * @Route("admin/patient", name="admin_patient_fiches")
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(PatientRepository $patientRepository,Request $request,PaginatorInterface $paginator)
    {
        $pagination=$paginator->paginate($patientRepository->findAllPaginator(),$request->query->getInt('page',1),10);
        return $this->render('admin/patient/index.html.twig', [
            'patients'=>$pagination
        ]);
    }


    /**
     * @Route("/admin/patient/show/{id}", name="admin_patient_show")
     * @IsGranted("ROLE_ADMIN")
     */
    public function patientShow(Patient $patient)
    {
        return $this->render('admin/patient/show.html.twig', [
            'patient'=>$patient
        ]);
    }


    /**
     * @Route("/admin/patient/edit/patient/{id}", name="admin_patient_edit")
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(Request $request,EntityManagerInterface $manager)
    {
        return $this->render('admin/patient/edit.html.twig', [

        ]);
    }


    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @Route("/admin/patient/create",name="admin_patient_new")
     * @IsGranted("ROLE_ADMIN")
     * Function create
     * User: emayemba
     * Date: 25/10/2020
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request,EntityManagerInterface $manager)
    {
        $patient=new Patient();
        $form=$this->createForm(PatientType::class,$patient);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

            //création d'une famille par la création d'un nouveau patient si rien n'est select ,effacement de ce champ dans le patientype
            if(!$patient->getFamily()){
                $famille=new Family();
                //strtoupper mettre le nom en majuscule
                $famille->setFamilyName(strtoupper($patient->getLastName()));
                $famille->addBeneficiaire($patient);
                $manager->persist($famille);
            }
            $manager->persist($patient);
            $manager->flush();
            $this->addFlash('success','ATTENTION  : Le patient '.' '.$patient->getFirstName().' '.$patient->getLastName().' '.' a été créé');
            return $this->redirectToRoute('admin_home');
        }
        return $this->render('admin/patient/create.html.twig',[
            'form'=>$form->createView()
        ]);

    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @Route("/admin/patient/update/{id}",name="admin_patient_edit")
     * @IsGranted("ROLE_ADMIN")
     * Function create
     * User: emayemba
     * Date: 25/10/2020
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request,EntityManagerInterface $manager,Patient $patient)
    {
        $form=$this->createForm(PatientEditType::class,$patient);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($patient);
            $manager->flush();
            $this->addFlash('success','ATTENTION  : Le patient '.' '.$patient->getFirstName().' '.$patient->getLastName().' '.' a été modifié');
            return $this->redirectToRoute('admin_home');
        }

        return $this->render('admin/patient/edit.html.twig',[
            'form'=>$form->createView(),
            'patient'=>$patient
        ]);

    }

    /**
     * @param PatientRepository $patientRepository
     * @Route("/admin/patients/listPdf",name="admin_patient_pdf")
     * Permet l'impression de la liste contenant tous les bénéficiaires (patients )
     * @param Request $request
     * @return Response
     */
    public function allPatientPdf(PatientRepository $patientRepository,Request $request){

        $patients=$patientRepository->findAllPatientsPdf();

        // on définit les options
        $options = new Options();
        //police par défaut
        $options->set('defaultFont', 'Courier');
        $options->setIsRemoteEnabled(true);

        //On instancie dompdf
        $dompdf = new Dompdf($options);

        //on génère le html
        $html=$this->renderView('admin/patient/patientsPdf.html.twig', [
            'patients'=>$patients,

        ]);

        $dompdf->loadHtml($html);

        $dompdf->setPaper('A4','portrait');

        // (Optional) Setup the paper size and orientation
        //     $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as pdf
        $dompdf->render();

        // on génère un fichier
        $fichier='ListeBénéficiares'.'pdf';

        // Output the generated pdf to Browser ,on envoie le pdf au navigateur ,mais attention méthode de stream à définir

        $dompdf->stream($fichier,[
            'attachment'=>true
        ]);
        //La response
        return new Response();


    }


}
