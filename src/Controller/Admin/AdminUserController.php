<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\User\UserType;
use App\Form\UserEditType;
use App\Repository\UserRepository;
use App\Service\Email\UpdateUserEmail;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class AdminUserController extends AbstractController
{
    /**
     * @Route("/admin/user", name="admin_user_index")
     * Liste des utilisateurs
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(UserRepository $userRepository,PaginatorInterface $paginator,Request $request)
    {
        $pagination=$paginator->paginate($userRepository->findAllOrderBy(),$request->query->getInt('page',1),5);
        return $this->render('admin/user/index.html.twig', [
           // 'users'=>$userRepository->findAll()
            'users'=>$pagination
        ]);
    }

    /**
     * @Route("/admin/user/show/detail/{id}", name="admin_user_show")
     * @IsGranted("ROLE_ADMIN")
     */
    public function userShow(User $user)
    {
        return $this->render('admin/user/show.html.twig', [
            'user'=>$user
        ]);
    }


    /**
     * @param User $user
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @Route("/admin/user/edit/user/{id}", name="admin_user_edit")
     * @IsGranted("ROLE_ADMIN")
     * Function userEdit
     * User: emayemba
     * Date: 25/10/2020
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function userEdit(User $user, Request $request, EntityManagerInterface $manager, UpdateUserEmail $updateUserEmail)
    {

        $form=$this->createForm(UserEditType::class,$user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            //gestion de family dans user

            // Je parcours la liste getFamilles et je persiste chaque objet de cette liste
            foreach ($user->getFamilies() as $family){
                $family->addUser($user);
                $manager->persist($family);
            }
            //Je persiste mon user
            $manager->persist($user);
            $manager->flush();
            $email=$user->getEmail();

            //je fais appel au service Email d'updatePateientEmail
            $updateUserEmail->sendEmail($email);

           /* $emailToSend=(new TemplatedEmail())
                   ->from('noreply@tulipa.fr')
                   ->to($email)
                   ->priority(Email::PRIORITY_NORMAL)
                   ->subject('Validation de votre compte Tulipa.fr pour AD SENIORS')
                   ->cc('indigo.goindigo@yopmail.com')
                   ->htmlTemplate('emails/validationCompte.html.twig');
             $mailer->send($emailToSend);*/

             $this->addFlash('success', 'Le compte de '.''.$user->getFirstName().''.$user->getLastName().''.'a été mis à jour:  ');
            return $this ->redirectToRoute('admin_user_index');
        }

        return $this->render('admin/user/edit.html.twig', [
            'user'=>$user,
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/admin/user/fiches/{id}", name="admin_user_fiches")
     * @IsGranted("ROLE_ADMIN")
     */
    public function userFicheList(User $user)
    {
        return $this->render('admin/user/fiches.html.twig', [
            'user'=>$user
        ]);
    }

    /*
     * Function testPdf
     * User: emayemba
     * Date: 31/10/2020
     * télécharge le fichier qui est dans le dossier document
     * @Route("/test/{id}",name="admin_pdf_test")
     */
    /*public function testPdf($id){
        $projectRoot = $this->getParameter('kernel.project_dir');
        $filename = $id;
        return $this->file( $projectRoot.'/public/documents/'.$filename );

    }*/

}
