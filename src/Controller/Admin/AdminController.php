<?php

namespace App\Controller\Admin;

use App\Entity\Patient;
use App\Form\Patient\PatientType;
use App\Repository\FicheRepository;
use App\Repository\InterventionRepository;
use App\Repository\PatientRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin/home", name="admin_home")
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(FicheRepository $ficheRepository ,PatientRepository $patientRepository,
                          InterventionRepository $interventionRepository,UserRepository $userRepository)
    {
        return $this->render('admin/home.html.twig', [
            'fiches'=>$ficheRepository->findAllLastFicheAdmin(),
            'allFiches'=>$ficheRepository->findAll(),
            'patients'=>$patientRepository->findAll(),
            'users'=>$userRepository->findAll(),
            'interventions'=>$interventionRepository->findAll()

        ]);
    }

    /**
     * Function myaccount
     * @Route("/admin/MyAccount/home",name="admin_account")
     * @IsGranted("ROLE_ADMIN")
     * User: emayemba
     * Date: 26/10/2020
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function myaccount(){

        return $this->render('admin/myAccount.html.twig',[
            'user'=>$this->getUser()
        ]);
    }


}
