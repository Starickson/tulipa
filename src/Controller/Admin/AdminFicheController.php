<?php

namespace App\Controller\Admin;

use App\Entity\Fiche;
use App\Entity\Patient;
use App\Form\FicheType;
use App\Repository\FicheRepository;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Dompdf\Options;
use Knp\Bundle\SnappyBundle\DependencyInjection\KnpSnappyExtension;
use Knp\Bundle\SnappyBundle\KnpSnappyBundle;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Snappy\Pdf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminFicheController extends AbstractController
{

    /**
     * @Route("/admin/fiche", name="admin_fiche")
     * @IsGranted("ROLE_ADMIN")
     */
    public function indexDate(FicheRepository $ficheRepository ,Request $request,PaginatorInterface $paginator)
    {
        $pagination = $paginator->paginate(
            $ficheRepository->findAllFichesPaginator(), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        return $this->render('admin/fiche/index.html.twig', [
            'fiches'=>$pagination
        ]);
    }


    /**
     * @Route("admin/fiche/transmissions/list/{id}", name="admin_fiche_transmissions")
     * @IsGranted("ROLE_ADMIN")
     */
    public function transmis(Patient $patient,PaginatorInterface  $paginator,FicheRepository  $ficheRepository,Request $request)
    {
        $pagination = $paginator->paginate(
            $ficheRepository->fichiersPatientPaginator($patient), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        return $this->render('admin/fiche/transmissions.html.twig', [
            'fiches'=>$pagination,
            'patient'=>$patient
        ]);

    }


    /**
     * Function patienFicheFiler
     * @Route("/admin/fiches/patient/{id}",name="admin_patientFiche_filter")
     * @IsGranted("ROLE_ADMIN")
     * User: emayemba
     * Date: 26/10/2020
     * Télécharger en pdf les fiches d'un patient sur une ^période donnée avec Dompdf
     * @return Response
     */
    public function patienFicheFiler(Patient $patient ,FicheRepository $ficheRepository,Request $request,Pdf $pdf){


        $date1=$request->get('_date1');
        $date2=$request->get('_date2');
        //Si la date de fin n'est pas définie alors je la fixe à today
        if(!$date2) {
            $date2 = date('Y-m-d');
        }
        $fiches=$ficheRepository->fichePatienfilter($patient,$date1,$date2);

        // on définit les options
        $options = new Options();
        //police par défaut
        $options->set('defaultFont', 'Courier');
        $options->setIsRemoteEnabled(true);

        //On instancie dompdf
        $dompdf = new Dompdf($options);

        //on génère le html
        $html=$this->renderView('admin/fiche/transmissionsFiltre.html.twig', [
            'fiches'=>$fiches,
            'patient'=>$patient
        ]);

        $dompdf->loadHtml($html);

        $dompdf->setPaper('A4','portrait');

      // (Optional) Setup the paper size and orientation
      //     $dompdf->setPaper('A4', 'landscape');

      // Render the HTML as pdf
        $dompdf->render();

        // on génère un fichier
        $fichier='Periode'.'pdf';

       // Output the generated pdf to Browser ,on envoie le pdf au navigateur ,mais attention méthode de stream à définir

        $dompdf->stream($fichier,[
            'attachment'=>true
        ]);
      //La response
        return new Response();

    }


    /**
     * @param Patient $patient
     * Function show
     * @Route("/admin/admin/fiche/show/{id}", name="admin_fiche_show", methods={"GET"})
     * détail d'une fiche donnée
     * User: emayemba
     * Date: 28/10/2020
     * @return Response
     */
    public function show(Fiche $fiche): Response
    {
        return $this->render('admin/fiche/show.html.twig', [
            'fiche' => $fiche,
        ]);
    }




    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @Route("/admin/fiche_create/admin_comment/new/{id}", name="admin_fiche_create")
     * @IsGranted("ROLE_ADMIN")
     * @param Patient $patient
     * Function createFiche
     * User: emayemba
     * Date: 24/10/2020
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createFiche(Request $request,EntityManagerInterface $manager,Patient $patient,FicheRepository $ficheRepository){
        $fiche=new Fiche();
        $user=$this->getUser();
        $form=$this->createForm(FicheType::class,$fiche);
        $fiche->setUser($user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $fiche->setPatient($patient);

            $manager->persist($fiche);
            $manager->flush();
            $this->addFlash('success','Votre commentaire a été  validé ');
            return $this->redirectToRoute('admin_home');
        }
        return $this ->render('admin/fiche/new_fiche.html.twig',[
            'form'=>$form->createView(),
            'patient'=>$patient,
            'user'=>$user,
            /*va afficher les 10 dernieres fiches de ce patient */
            'fichiers'=>$ficheRepository->findFichiersPatient($patient)

        ]);
    }


}
