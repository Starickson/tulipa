<?php

namespace App\Controller\Admin;

use App\Entity\Patient;
use App\Repository\FicheRepository;
use App\Repository\InterventionRepository;
use App\Repository\PatientRepository;
use Dompdf\Dompdf;
use Dompdf\Options;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Snappy\Pdf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminInterventionController extends AbstractController
{
    /**
     * @Route("/admin/intervention", name="admin_intervention")
     * liste de toutes les intervention qq soit les patients
     */
    public function index(InterventionRepository $interventionRepository)
    {
        return $this->render('admin/intervention/index.html.twig', [
            'interventions' => $interventionRepository->findAll()
        ]);
    }

    /**
     * @Route("/admin/interventions/listing", name="admin_interventions")
     * liste de toutes les intervention qq soit les patients sur le lient Historique Diag Int
     */
    public function list(InterventionRepository $interventionRepository,Request $request,PaginatorInterface  $paginator)
    {
        $interventions=$paginator->paginate($interventionRepository->findAllLastInterventionsPaginator(),$request->query->getInt('page',1),20);
        return $this->render('admin/intervention/list.html.twig', [
            'interventions' => $interventions
        ]);
    }

    /**
     * @param PatientRepository $patientRepository
     * @Route("/admin/intervention/patient/{id}",name="admin_intervention_recap")
     * @IsGranted("ROLE_ADMIN")
     * Function recapIntervention
     * Affiche la liste des intervention pour ce patient id
     * User: emayemba
     * Date: 04/11/2020
     * function récapitulatif pour récupérer l'ensemble des intervention d'un patient
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function recapIntervention(InterventionRepository $interventionRepository ,Patient $patient){
        //autre solution parameter converter
        $interventions=$interventionRepository->findByPatient($patient);
        return $this->render('admin/intervention/recapIntervention.html.twig',[
            'patient'=>$patient,
            'interventions'=>$interventions
        ]);
    }

    /**
     * @Route("/admin/intervention/patient/pdf/{id}",name="admin_intervention_patient_pdf")
     * @IsGranted("ROLE_ADMIN")
     * Permet de générer un pdf des intervention du patient  au format voulu.Ressemble aux fiches du patient en pdf
     * User: emayemba
     * Télécharger en pdf les intervention d'un patient sur une ^période donnée avec Dompdf
     * @return Response
     */
    public function patienInterventionFilter(Patient $patient ,InterventionRepository $interventionRepository,Request $request){

        $date1=$request->get('_date1');
        $date2=$request->get('_date2');
        $interventions=$interventionRepository->interventionPatienfilter($patient,$date1,$date2);

        // on définit les options
        $options = new Options();
        //police par défaut
        $options->set('defaultFont', 'Courier');
        $options->setIsRemoteEnabled(true);

        //On instancie dompdf
        $dompdf = new Dompdf($options);

        //on génère le html
        $html=$this->renderView('admin/intervention/diagrammeIntervention.html.twig', [
            'interventions'=>$interventions,
            'patient'=>$patient,
            'user'=>$this->getUser()
        ]);

        $dompdf->loadHtml($html);

    /*   RAPPEL DIMENSIONS DE BASE

       "a3" => array(0,0,841.89,1190.55),
        "a4" => array(0,0,595.28,841.89),*/

      /*  Taille A3 de base :

      $customPaper = array(0,0,841.89,1190.55);
        $dompdf->setPaper($customPaper);  */

        /* Dimension appliquée */

        $dompdf->setPaper('a2', 'landscape');

        // Render the HTML as pdf
        $dompdf->render();

        $date=date('Ymd');

        // on génère un fichier à la date du jour
        $fichier='DIAGRAMME DE SOINS_'.$patient->getLastName().'_'.$date;

        // Output the generated pdf to Browser ,on envoie le pdf au navigateur ,mais attention méthode de stream à définir
        $dompdf->stream($fichier,[
            'attachment'=>true
            //'the_inquiry.pdf'
        ]);
        //La response
        return new Response();

    }






}
