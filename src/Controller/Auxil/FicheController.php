<?php

namespace App\Controller\Auxil;

use App\Entity\Fiche;
use App\Entity\Patient;
use App\Form\FicheType;
use App\Repository\FicheRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FicheController extends AbstractController
{
    /**
     * @Route("/auxil/fiche", name="fiche_index")
     * liste des 3 dernieres fiches mais template inactif à configurer
     * @IsGranted("ROLE_USER")
     */
    public function index(FicheRepository $ficheRepository)
    {
        return $this->render('auxi/fiche/index.html.twig', [
            'fiches'=>$ficheRepository->findAllLastFiche()
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @Route("/auxil/fiche/new/{id}", name="fiche_create")
     * @IsGranted("ROLE_USER")
     * @param Patient $patient
     * Function createFiche
     * User: emayemba
     * Date: 24/10/2020
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createFiche(Request $request,EntityManagerInterface $manager,Patient $patient,FicheRepository $ficheRepository){
        $fiche=new Fiche();
        $user=$this->getUser();
        $form=$this->createForm(FicheType::class,$fiche);
        $fiche->setUser($user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $fiche->setPatient($patient);

            $manager->persist($fiche);
            $manager->flush();
            $this->addFlash('success','Votre commentaire a été  validé ');
            return $this->redirectToRoute('auxil_home');
        }
        return $this ->render('auxi/fiche/new_fiche.html.twig',[
            'form'=>$form->createView(),
            'patient'=>$patient,
            'user'=>$user,
            /*va afficher les 10 dernieres fiches de ce patient */
            'fichiers'=>$ficheRepository->findFichiersPatient($patient)

        ]);
    }
}
