<?php

namespace App\Controller\Auxil;

use App\Entity\Intervention;
use App\Entity\Patient;
use App\Form\Intervention\InterventionType;
use App\Repository\InterventionRepository;
use App\Repository\PatientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class InterventionController extends AbstractController
{
    /**
     * @Route("/auxil/intervention", name="intervention_index")
     */
    public function index(InterventionRepository $interventionRepository)
    {
        return $this->render('auxi/intervention/index.html.twig', [
            'interventions' => $interventionRepository->findAll(),
        ]);
    }


    /**
     * @param PatientRepository $patientRepository
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @Route("auxil/intervention/new/{id}",name="intervention_create")
     * Function createIntervention
     * User: emayemba
     * Date: 04/11/2020
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createIntervention($id,PatientRepository $patientRepository,Request $request,EntityManagerInterface $manager)
    {
        $interventon=new Intervention();
        $user=$this->getUser();
        $patient= $patientRepository->find($id);
        $form=$this->createForm(InterventionType::class,$interventon);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $interventon->setPatient($patient);
            $interventon->setUser($user);
            $manager->persist($interventon);
            $manager->flush();

            $this->addFlash('success','Votre intervention a été créée');
            return $this->redirectToRoute('auxil_home');
        }
        return $this->render('auxi/intervention/newIntervention.html.twig',[
            'form'=>$form->createView(),
            'patient'=>$patient,
            'user'=>$this->getUser()

        ]);



    }

    /**
     * @param PatientRepository $patientRepository
     * @Route("/auxil/intervention/patient/{id}",name="intervention_recap")
     * Function recapIntervention
     * User: emayemba
     * Date: 04/11/2020
     * function récapitulatif pour récupérer l'ensemble des intervention d'un patient
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function recapIntervention(InterventionRepository $interventionRepository ,Patient $patient){
        //autre solution parameter converter
         $interventions=$interventionRepository->findByPatient($patient);
        return $this->render('auxi/intervention/recapIntervention.html.twig',[
            'patient'=>$patient,
            'interventions'=>$interventions
        ]);
    }
}


