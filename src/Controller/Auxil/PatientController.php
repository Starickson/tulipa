<?php

namespace App\Controller\Auxil;

use App\Form\SearchType;
use App\Repository\PatientRepository;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PatientController extends AbstractController
{
    /**
     * @Route("auxil/patient", name="patient_index")
     * @IsGranted("ROLE_USER")
     * affiches tous les patients avec KnpPaginator
     * ajout de paginator le 28/10/20
     */
    public function index(PaginatorInterface $paginator,Request $request,PatientRepository $patientRepository)
    {
        $form=$this->createForm(SearchType::class);
        $pagination=$paginator->paginate( $patientRepository->findAllPatients(), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/);

        return $this->render('auxi/patient/index.html.twig', [
           // 'patients'=>$patientRepository->findAll(),
            'patients'=>$pagination,
            'form'=>$form->createView()

        ]);
    }

    /**
     * @param Request $request
     * @param PatientRepository $patientRepository
     * @Route("auxil/patient/search",name="patient_search")
     * @IsGranted("ROLE_USER")
     * Permet la recherche de patients dans un formulaire intégré au template de patient_index
     * Function searchPatient
     * User: emayemba
     * Date: 24/10/2020
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchPatient(Request $request,PatientRepository $patientRepository,PaginatorInterface $paginator){
     $form=$this->createForm(SearchType::class);
     $form->handleRequest($request);

     if($form->isSubmitted() && $form->isValid()){
         $name=$form->get('lastName')->getData();
         $patientsRepo=$patientRepository->findPatientsByName($name);

         if(empty($patientsRepo->getResult())){

             $this->addFlash('erreur', 'Aucun article contenant ce mot clé dans le titre n\'a été trouvé, essayez en un autre.');
             return $this->render('auxi/patient/nopatients.html.twig');
         }
         $pagination=$paginator->paginate( $patientsRepo, /* query NOT result */
             $request->query->getInt('page', 1), /*page number*/
             10 /*limit per page*/);
         return $this->render('auxi/patient/index.html.twig',[
             'patients'=>$pagination,
             'form'=>$form->createView()
         ]);
     }
        return $this->render('auxi/patient/index.html.twig', [
            'form'=>$form->createView(),

        ]);
    }




}
