<?php

namespace App\Controller\Auxil;

use App\Repository\FicheRepository;
use App\Repository\InterventionRepository;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AuxilController extends AbstractController
{
    /**
     * @Route("/auxil/home", name="auxil_home")
     * affiche une liste des 3 dernieres fiches créées à la pae home auxiliaire
     * @IsGranted("ROLE_USER")
     */
    public function index(FicheRepository $ficheRepository,InterventionRepository $interventionRepository)
    {
        return $this->render('auxi/auxil_home.html.twig', [
         'fiches'=>$ficheRepository->findAllLastFiche(),
         'interventions'=>$interventionRepository->findAllLastInterventions(),


        ]);
    }
}
