<?php

namespace App\Controller\famille;

use App\Entity\Patient;
use App\Repository\FicheRepository;
use App\Repository\PatientRepository;
use Dompdf\Dompdf;
use Dompdf\Options;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Snappy\Pdf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FamilleFicheController extends AbstractController
{
    /**
     * @Route("/family/fiche/{id}", name="family_fiches_index")
     * @IsGranted("ROLE_FAMILY")
     */
    public function index(Patient $patient,PaginatorInterface $paginator,Request $request,FicheRepository  $ficheRepository)
    {
        $fiches=$paginator->paginate($ficheRepository->fichiersPatientPaginator($patient),$request->query->getInt('page',1,10));
        return $this->render('family/fiche/index.html.twig', [
            'fiches' => $fiches,
            'patient'=>$patient
        ]);
    }

    /**
     * Function patienFicheFiler
     * @Route("/family/fiches/patient/{id}",name="family_patientFiche_filter")
     * @IsGranted("ROLE_FAMILY")
     * User: emayemba
     * Date: 15/11/2020
     * Télécharger en pdf les fiches d'un patient sur une période donnée avec Dompdf
     * @return Response
     */
    public function patienFicheFiler(Patient $patient ,FicheRepository $ficheRepository,Request $request){

        $date1=$request->get('_date1');
        $date2=$request->get('_date2');
        $fiches=$ficheRepository->fichePatienfilter($patient,$date1,$date2);

        // on définit les options
        $options = new Options();
        //police par défaut
        $options->set('defaultFont', 'Courier');
        $options->setIsRemoteEnabled(true);

        //On instancie dompdf
        $dompdf = new Dompdf($options);

        //on génère le html
        $html=$this->renderView('family/fiche/transmissionsFiltre.html.twig', [
            'fiches'=>$fiches,
            'patient'=>$patient
        ]);

        $dompdf->loadHtml($html);

        $dompdf->setPaper('A4','portrait');

        // (Optional) Setup the paper size and orientation
        //     $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as pdf
        $dompdf->render();

        // on génère un fichier
        $fichier='Periode'.'pdf';

        // Output the generated pdf to Browser ,on envoie le pdf au navigateur ,mais attention méthode de stream à définir

        $dompdf->stream($fichier,[
            'attachment'=>true
        ]);
        //La response
        return new Response();

    }






}
