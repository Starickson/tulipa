<?php

namespace App\Controller\famille;

use App\Entity\Patient;
use App\Repository\InterventionRepository;
use App\Repository\PatientRepository;
use Dompdf\Dompdf;
use Dompdf\Options;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FamilleInterventionController extends AbstractController
{
    /**
     * @param InterventionRepository $interventionRepository
     * @param Patient $patient
     * @return Response
     * @Route("/family/intervention/{id}",name="family_interventions_index")
     * @IsGranted("ROLE_FAMILY")
     */
    public function recapIntervention(InterventionRepository $interventionRepository ,Patient $patient){
        //autre solution parameter converter
        $interventions=$interventionRepository->findByPatient($patient);
        return $this->render('family/interventions/index.html.twig',[
            'patient'=>$patient,
            'interventions'=>$interventions
        ]);
    }

    /**
     * @Route("/family/intervention/patient/pdf/{id}",name="family_intervention_patient_pdf")
     * @IsGranted("ROLE_FAMILY")
     * Permet de générer un pdf des intervention du patient  au format voulu.Ressemble aux fiches du patient en pdf
     * User: emayemba
     * Télécharger en pdf les intervention d'un patient sur une période donnée avec Dompdf
     * @return Response
     */
    public function patienInterventionFilter(Patient $patient ,InterventionRepository $interventionRepository,Request $request){

        $date1=$request->get('_date1');
        $date2=$request->get('_date2');
        $interventions=$interventionRepository->interventionPatienfilter($patient,$date1,$date2);

        // on définit les options
        $options = new Options();
        //police par défaut
        $options->set('defaultFont', 'Courier');
        $options->setIsRemoteEnabled(true);

        //On instancie dompdf
        $dompdf = new Dompdf($options);

        //on génère le html
        $html=$this->renderView('family/interventions/diagrammeIntervention.html.twig', [
            'interventions'=>$interventions,
            'patient'=>$patient,
            'user'=>$this->getUser()
        ]);

        $dompdf->loadHtml($html);

        /*    "a3" => array(0,0,841.89,1190.55),
            "a4" => array(0,0,595.28,841.89),*/

        $customPaper = array(0,0,841.89,1190.55);
        $dompdf->setPaper($customPaper);
        // $dompdf->setPaper('A3','landscape');

        // (Optional) Setup the paper size and orientation
        //     $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as pdf
        $dompdf->render();

        // on génère un fichier
        $fichier='DIAGRAMME DE SOIN'.'pdf';


        // Output the generated pdf to Browser ,on envoie le pdf au navigateur ,mais attention méthode de stream à définir

        $dompdf->stream($fichier,[
            'attachment'=>true
        ]);
        //La response
        return new Response();

    }









}
