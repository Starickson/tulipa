<?php

namespace App\Controller\famille;

use App\Entity\Patient;
use App\Repository\FamilyRepository;
use App\Repository\PatientRepository;
use App\Service\Family\FamilyService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FamillePatientController extends AbstractController
{
    /**
     * @Route("/family/patients", name="family_patients")
     * @IsGranted("ROLE_FAMILY")
     * liste des membres de famille du user
     */
    public function index(FamilyRepository $familyRepository,PatientRepository $patientRepository,FamilyService $familyService)
    {
       $user=$this->getUser();
        return $this->render('family/patient/index.html.twig', [
            'user'=>$user
        ]);
    }

    /**
     * @Route("/family/patient/show/{id}", name="family_patient_show")
     * @IsGranted("ROLE_FAMILY")
     * détail du patient caractérisé par son id
     */
    public function patientShow(Patient $patient)
    {
        return $this->render('family/patient/show.html.twig', [
            'patient'=>$patient
        ]);
    }




}
