<?php

namespace App\Repository;

use App\Entity\Intervention;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Intervention|null find($id, $lockMode = null, $lockVersion = null)
 * @method Intervention|null findOneBy(array $criteria, array $orderBy = null)
 * @method Intervention[]    findAll()
 * @method Intervention[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InterventionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Intervention::class);
    }


    /**
     * @param $patient
     * Function findByPatient
     * afficher une liste d'intervention d'un patient avec un max result à 10
     * User: emayemba
     * Date: 04/11/2020
     * @return int|mixed|string
     */
    public function findByPatient($patient){
        return $this->createQueryBuilder('i')
                    ->andWhere('i.patient = :val')
                    ->setParameter('val',$patient)
                    ->orderBy('i.createdAt','ASC')
                    ->setMaxResults(32)
                    ->getQuery()
                    ->getResult();

    }

    /**
     * Function findAllLastInterventions
     * liste des 3 dernieres intervention réalisées .
     * User: emayemba
     * Date: 04/11/2020
     * @return int|mixed|string
     */
    public function findAllLastInterventions(){
        return $this->createQueryBuilder('i')
            ->orderBy('i.createdAt', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult()
            ;
    }

    /* function de filtre en fonction des dates sur un utilisteur */
    public function interventionPatienfilter($patient,$date1,$date2){
        return $this->createQueryBuilder('i')
            ->select('i')
            ->where('i.patient = :patient')
            ->andwhere('i.createdAt BETWEEN :date1 and :date2')
            ->setParameter('patient',$patient)
            ->setParameter('date1',$date1)
            ->setParameter('date2',$date2)
            ->groupBy('i' )
            ->orderBy('i.createdAt','ASC')
            ->getQuery()
            ->getResult();

    }

    /**
     * @return Query
     * Retourne toutes les interventions pour le paginator demandé dans le controlleur adminInterventionController
     */
    public function findAllLastInterventionsPaginator():Query{
        return $this->createQueryBuilder('i')
            ->orderBy('i.createdAt', 'DESC')
            ->getQuery()
            ;
    }



    // /**
    //  * @return Intervention[] Returns an array of Intervention objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Intervention
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
