<?php

namespace App\Repository;

use App\Entity\Patient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Patient|null find($id, $lockMode = null, $lockVersion = null)
 * @method Patient|null findOneBy(array $criteria, array $orderBy = null)
 * @method Patient[]    findAll()
 * @method Patient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Patient::class);
    }

    public function findAllPaginator():Query{
        return $this->createQueryBuilder('p')
                    ->orderBy('p.lastName','ASC')
                    ->getQuery();
    }

    public function findPatientsByName($value):Query
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.lastName LIKE :val')
            ->setParameter('val', '%'.$value.'%')
            ->setMaxResults(10)
            ->getQuery()
           //pour laisser la gestion à paginator  ->getResult()
            ;
    }


    /**
     * Function findAllPatients
     * Même requête q'un findAll mais renvoie toute la Query qui sera 'sort' par paginator
     * Utilisation dans le controller PatientController
     * User: emayemba
     * Date: 28/10/2020
     * @return Query
     */
    public function findAllPatients():Query{
        return $this->createQueryBuilder('p')
                    ->getQuery();

}
    public function findAllPatientsPdf(){
        return $this->createQueryBuilder('p')
        ->orderBy('p.lastName','ASC')
        ->getQuery()
        ->getResult();
     }






    // /**
    //  * @return Patient[] Returns an array of Patient objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Patient
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
