<?php

namespace App\Repository;

use App\Entity\Fiche;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Fiche|null find($id, $lockMode = null, $lockVersion = null)
 * @method Fiche|null findOneBy(array $criteria, array $orderBy = null)
 * @method Fiche[]    findAll()
 * @method Fiche[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FicheRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Fiche::class);
    }

    public function findAllFichesPaginator():Query{
        return $this->createQueryBuilder('f')
                    ->orderBy('f.createdAt','DESC')
                    ->getQuery();
    }

    /**
     * Function findAllLastFiche
     * User: emayemba
     * Affiche toutes les fiches filtrés à la createdAt sur la page home du user et de linfirmmier (roles )
     * Date: 24/10/2020
     * @return int|mixed|string
     */
    public function findAllLastFiche()
    {
        return $this->createQueryBuilder('f')
            ->orderBy('f.createdAt', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult()
            ;
    }

  /* supprimer une des deux fonction DOUBLON*/
    /**
     * Function findAllLastFiche
     * User: emayemba
     * Affiche toutes les fiches filtrés à la createdAt sur la page home de l'admin
     * Date: 24/10/2020
     * @return int|mixed|string
     */
    public function findAllLastFicheAdmin()
    {
        return $this->createQueryBuilder('f')
            ->orderBy('f.createdAt', 'DESC')
            ->setMaxResults(6)
            ->getQuery()
            ->getResult()
            ;
    }

    /* function de filtre en fonction des dates sur toute laliste */
    public function ficheAllfilter($date1,$date2):Query{
        return $this->createQueryBuilder('f')
            ->andwhere('f.createdAt BETWEEN :date1 and :date2')
            ->setParameter('date1',$date1)
            ->setParameter('date2',$date2)
            ->orderBy('f.createdAt','DESC')
            ->getQuery();
    }


/* function de filtre en fonction des dates sur un utilisteur */
    public function fichePatienfilter($patient,$date1,$date2){
        return $this->createQueryBuilder('f')
                   ->select('f')
                   ->where('f.patient = :patient')
                   ->andwhere('f.createdAt BETWEEN :date1 and :date2')
                   ->setParameter('patient',$patient)
                   ->setParameter('date1',$date1)
                   ->setParameter('date2',$date2)
                   ->groupBy('f' )
                   ->orderBy('f.createdAt','DESC')
                   ->getQuery()
                   ->getResult();

    }


    /**
     * @param $value
     * Function fichesUser
     * Pour paginator ,permet de récup toutes les fiches d'un User
     * User: emayemba
     * Date: 28/10/2020
     * @return int|mixed|string
     */
    public function fichesUser($value):Query{
        return $this->createQueryBuilder('f')
            ->join('f.patient','p')
            ->join('f.user','u')
            ->andWhere('u.id = :val')
            ->setParameter('val',$value->getId())
            ->groupBy('f')
            ->orderBy('f.createdAt','DESC')
            ->getQuery();


    }

    /**
     * @param $patient
     * Function findByPatient
     * afficher une liste de fichier d'un patient avec un max result à 10
     * User: emayemba
     * Date: 04/11/2020
     * @return int|mixed|string
     */
    public function findFichiersPatient($patient){
        return $this->createQueryBuilder('f')
            ->andWhere('f.patient = :val')
            ->setParameter('val',$patient)
            ->orderBy('f.createdAt','DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $patient
     * Function dans le controller familyfichecontroller
     * afficher une liste de fichier d'un patient avec un max result à 10 pour un listing paginator
     * User: emayemba
     * Date: 15/11/2020
     * @return int|mixed|string
     */
    public function fichiersPatientPaginator($patient):Query{
        return $this->createQueryBuilder('f')
            ->andWhere('f.patient = :val')
            ->setParameter('val',$patient)
            ->orderBy('f.createdAt','DESC')
            ->setMaxResults(10)
            ->getQuery();

    }


    // /**
    //  * @return Fiche[] Returns an array of Fiche objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Fiche
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
