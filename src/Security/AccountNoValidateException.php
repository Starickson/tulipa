<?php

namespace App\Security;

use Symfony\Component\Security\Core\Exception\AccountStatusException;

class AccountNoValidateException extends AccountStatusException

{
    /**
     * Function getMessageKey
     * User: emayemba
     * Date: 23/10/2020
     * @return string
     */
    public function getMessageKey()
    {
        return 'Votre compte n \'a pas encore été validé par nos services , merci de votre compréhension';
    }

}
