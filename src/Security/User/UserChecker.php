<?php
namespace App\Security\User;

use App\Entity\User as AppUser;
use App\Security\AccountNoValidateException;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user)
    {

        if (!$user instanceof AppUser) {
            return;
        }

        if ($user->getIsValidate()===false) {
// the message passed to this exception is meant to be displayed to the user
            throw new AccountNoValidateException();
        }
    }

    public function checkPostAuth(UserInterface $user)
    {
        if (!$user instanceof AppUser) {
            return;
        }

    }
}
