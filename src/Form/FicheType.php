<?php

namespace App\Form;

use App\Entity\Fiche;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class FicheType extends AbstractType
{
    //j'introduit une varibale securite et j'injecte la classe SECURITY pour accéder à mon user en cours
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    //fin
    //function classique
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();

        if ($user->getIsNurse() || $user->getRoles()[0]==='ROLE_ADMIN'  ) {
            $builder->add('nurseNote', TextareaType::class, [
                'label' => 'Note de l\' infirmier(ère) ou du service de gestion ',
                'required' => false,
            ])
                ->add('comment', HiddenType::class, [
                    'label' => 'Commentaire',
                    'empty_data' => '-',
                ]);
        } else {
            ;
            $builder->add('comment', TextareaType::class, [
                'label' => 'Commentaire'
            ]);

        }

    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Fiche::class,
        ]);
    }
}
