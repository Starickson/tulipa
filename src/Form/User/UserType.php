<?php

namespace App\Form\User;

use App\Entity\Family;
use App\Entity\User;
use phpDocumentor\Reflection\Types\Collection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastName',TextType::class,[
                'label'=>'Nom'
            ])
            ->add('firstName',TextType::class,[
                'label'=>'Prénom'
            ])
            ->add('email',EmailType::class,[
                'label'=>'Email'
            ])
            ->add('phone',TextType::class,[
                'label'=>'Téléphone'
            ])

            /*->add('createdAt',DateTimeType::class,[
                'label'=>'Créé le /Mise à jour le ',
                'widget' => 'single_text',
            ])*/

            ->add('informations',TextareaType::class,[
                'label'=>'Informations complémentaires',
                'required'=>false,
                'attr'=>[
                    'placeholder'=>'Merci de fournir un complément d\'information (ex: dans le cas des familles, nous fournir le nom et prénom du membre de votre famille )'
                ]
            ])

            ->add('roles', HiddenType::class, [
                'data' => 'ROLE_USER'
            ])

            ->add('password',RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe doivent être identiques.',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => ['label' => 'Mot de passe'],
                'second_options' => ['label' => 'Mot de passe à répéter ']
                ])



        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
