<?php

namespace App\Form\Intervention;

use App\Entity\Intervention;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InterventionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('douche_shampoing')
            ->add('toilette_complete')
            ->add('soinsDeBouche')
            ->add('soinsEsthetique')
            ->add('habillage')
            ->add('pyjama')
            ->add('onglesDeMains')
            ->add('cremeMassage')
            ->add('rasage')
            ->add('changmentDePosition')
            ->add('refectionLit')
            ->add('changementDeDrap')
            ->add('aspirateur')
            ->add('vaisselle')
            ->add('lessive')
            ->add('nettoyageSanitaire')
            ->add('rangementPlacardVetement')
            ->add('repassage')
            ->add('depoussierageMeubles')
            ->add('nettoyageFrigo')
            ->add('nettoyageMicroOnde')
            ->add('descentePoubelle')
            ->add('communication')
            ->add('courses')
            ->add('preparationPetitDejeunerLendemain')
            ->add('promenade')
            ->add('appreterLeGout')
            ->add('repas')
            ->add('priseMedicaments')
            ->add('selles')
            ->add('urine')
            ->add('etatCutaneFriction')


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Intervention::class,
        ]);
    }
}
