<?php

namespace App\Form;

use App\Entity\Family;
use App\Entity\User;
use Doctrine\DBAL\Types\ArrayType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder


            ->add('lastName',TextType::class,[
                'label'=>'Nom'
            ])
            ->add('firstName',TextType::class,[
                'label'=>'Prénom'])
            ->add('email',TextType::class,[
                'label'=>'Email'])
            ->add('phone',TextType::class,[
                'label'=>'Téléphone'])

            ->add('families',EntityType::class,[
                'class'=>Family::class,
                'choice_label'=>'familyName',
                'multiple'=>true,
                //'expanded'=>true  ->des checkboxs
                'required' => false
            ])
            ->add('is_validate',CheckboxType::class,[
                'label'=>'Valider l \'utilisateur',
                'required'=>false
            ])
            ->add('is_nurse',CheckboxType::class,[
                'label'=>'infirmier ? ',
                'required'=>false
            ])
           /* ->add('roles', CollectionType::class, [
                // each entry in the array will be an "email" field
                'entry_type' => TextType::class,
                'allow_add' => true,
                // these options are passed to each "email" type
                'entry_options' => [
                    'attr' => ['class' => 'roles'],
                ],
            ])*/

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
