<?php

namespace App\Form\Patient;

use App\Entity\Family;
use App\Entity\Patient;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PatientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gender',ChoiceType::class,[
                'label'=>'Sexe',
                'choices'  => [
                    'Masculin' => 'M',
                    'Féminin' => 'F',

                ],
            ])
            ->add('lastName',TextType::class,[
                'label'=>'Nom'
            ])
            ->add('firstName',TextType::class,[
                'label'=>'Prénom'
            ])
            ->add('phone',TextType::class,[
                'label'=>'Téléphone'
            ])
            ->add('adress',TextType::class,[
                'label'=>'Adresse'
            ])
            ->add('floor',TextType::class,[
                'label'=>'Etage'
            ])
            ->add('cp',TextType::class,[
                'label'=>'Code Postal'
            ])
            ->add('town',TextType::class,[
                'label'=>'Ville'
            ])
            ->add('code',TextType::class,[
                'label'=>'Code interne'
            ])

            ->add('is_active',CheckboxType::class,[
                'label'=>'Est actif',
                'required'=>false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Patient::class,
        ]);
    }
}
