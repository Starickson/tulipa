<?php

namespace App\Service\Email;


use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class  UpdateUserEmail
{
    public $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer=$mailer;
    }

    public function sendEmail($email){

        $emailToSend=(new TemplatedEmail())
            ->from('noreply@tulipa.fr')
            ->to($email)
            ->priority(Email::PRIORITY_NORMAL)
            ->subject('Validation de votre compte Tulipa.fr pour AD SENIORS')
            //->cc('noreply@yopmail.com')
            ->htmlTemplate('emails/validationCompte.html.twig');
        $this->mailer->send($emailToSend);
    }

}
