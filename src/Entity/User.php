<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity("email",message="Cette adresse mail est déjà utilisée")
 * @ORM\HasLifecycleCallbacks()
 *
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Email(message="Veuillez saisir un email ")
     */
    private $email;


    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Assert\Length(min=2,max=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $picture;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_validate;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=10,minMessage="10 chiffres au moins ",max=15,maxMessage="15 chiffres maximum ")
     */
    private $phone;

    /**
     * @ORM\Column(type="datetime")
     *
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=2,max=50,maxMessage="Votre nom dépasse la longueur  admise de 50 lettre ",minMessage="Votre nom doit comporter au mois deux lettres ")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=2,max=50,minMessage="Votre prénom doit comporter au moins 2 lettre ",maxMessage="Votre prénom ne doit pas dépasser 50 lettres ")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $roles;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(min=2,max=255,minMessage="Un minimum de 2 lettres svp ",maxMessage="Un maximum de 255 lettres svp")
     */
    private $informations;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $resetToken;

    /**
     * @ORM\OneToMany(targetEntity=Fiche::class, mappedBy="user")
     */
    private $fiches;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Assert\Length(max=10,maxMessage="10 lettres ou chiffres max ")
     */
    private $code;

    /**
     * @ORM\OneToMany(targetEntity=Intervention::class, mappedBy="user")
     */
    private $interventions;

//au départ  targetEntity=Family::class, mappedBy="users"
    /**
     * @ORM\ManyToMany(targetEntity=Family::class, inversedBy="users")
     */
    private $families;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isNurse;



    public function __construct()
    {
        $this->fiches = new ArrayCollection();
        $this->interventions = new ArrayCollection();
        $this->families = new ArrayCollection();


    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles()//: array
    {
        /* OLD :$roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';*/
        //return array_unique($roles);

        return [$this->roles];

    }

    /**
     * @param string $roles
     * Function setRoles
     * User: emayemba
     * Date: 22/10/2020
     * @return $this
     */
    public function setRoles(string $roles): self
    {
        if($roles===null){
         $this->roles="ROLE_USER";
        }else{
            $this->roles = $roles;
        }
        return $this;
    }


    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }


    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getIsValidate(): ?bool
    {
        return $this->is_validate;
    }

    public function setIsValidate(bool $is_validate): self
    {
        $this->is_validate = $is_validate;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }



    public function getInformations(): ?string
    {
        return $this->informations;
    }

    public function setInformations(?string $informations): self
    {
        $this->informations = $informations;

        return $this;
    }

    //le preupdate a été supprimé pour permettre l'update sans valeur par défaut
    /**
     * Function defaultValidate
     * @ORM\PrePersist()
     * User: emayemba
     * Date: 22/10/2020
     */
    public function defaultValidate(){
        $this->is_validate=false;
        return $this;
    }

    /**
     * Function defaultDate
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     * User: emayemba
     * Date: 23/10/2020
     */
    public function defaultDate(){
        $this->createdAt=new \DateTime();
        return $this;
    }

    /**
     * Function defaultAvatr
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     * User: emayemba
     * Date: 27/10/2020
     * @return $this
     */
    public function defaultAvatr(){
        $this->picture='avatar-neutre.png';
        return $this;
    }

    public function getResetToken(): ?string
    {
        return $this->resetToken;
    }

    public function setResetToken(?string $resetToken): self
    {
        $this->resetToken = $resetToken;

        return $this;
    }

    /**
     * @return Collection|Fiche[]
     */
    public function getFiches(): Collection
    {
        return $this->fiches;
    }

    public function addFich(Fiche $fich): self
    {
        if (!$this->fiches->contains($fich)) {
            $this->fiches[] = $fich;
            $fich->setUser($this);
        }

        return $this;
    }

    public function removeFich(Fiche $fich): self
    {
        if ($this->fiches->contains($fich)) {
            $this->fiches->removeElement($fich);
            // set the owning side to null (unless already changed)
            if ($fich->getUser() === $this) {
                $fich->setUser(null);
            }
        }

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection|Intervention[]
     */
    public function getInterventions(): Collection
    {
        return $this->interventions;
    }

    public function addIntervention(Intervention $intervention): self
    {
        if (!$this->interventions->contains($intervention)) {
            $this->interventions[] = $intervention;
            $intervention->setUser($this);
        }

        return $this;
    }

    public function removeIntervention(Intervention $intervention): self
    {
        if ($this->interventions->contains($intervention)) {
            $this->interventions->removeElement($intervention);
            // set the owning side to null (unless already changed)
            if ($intervention->getUser() === $this) {
                $intervention->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Family[]
     */
    public function getFamilies(): Collection
    {
        return $this->families;
    }

    public function addFamily(Family $family): self
    {
        if (!$this->families->contains($family)) {
            $this->families[] = $family;
            $family->addUser($this);
        }

        return $this;
    }

    public function removeFamily(Family $family): self
    {
        if ($this->families->contains($family)) {
            $this->families->removeElement($family);
            $family->removeUser($this);
        }

        return $this;
    }

    public function getIsNurse(): ?bool
    {
        return $this->isNurse;
    }

    public function setIsNurse(bool $isNurse): self
    {
        $this->isNurse = $isNurse;

        return $this;
    }

    /**
     *@ORM\PrePersist()
     */
    public function isNurseInitial(){
        if(!$this->isNurse){
            $this->isNurse=false;
        }
    }



}
