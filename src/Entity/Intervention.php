<?php

namespace App\Entity;

use App\Repository\InterventionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InterventionRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Intervention
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $douche_shampoing;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $toilette_complete;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $soinsDeBouche;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $soinsEsthetique;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $habillage;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $pyjama;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $onglesDeMains;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $cremeMassage;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $rasage;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $changmentDePosition;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $refectionLit;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $changementDeDrap;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $aspirateur;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $vaisselle;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $lessive;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $nettoyageSanitaire;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $rangementPlacardVetement;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $repassage;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $depoussierageMeubles;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $nettoyageFrigo;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $nettoyageMicroOnde;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $descentePoubelle;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $communication;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $courses;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $preparationPetitDejeunerLendemain;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $promenade;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $appreterLeGout;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $repas;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $priseMedicaments;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $selles;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $urine;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $etatCutaneFriction;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Patient::class, inversedBy="intervention")
     */
    private $patient;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="intervention")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDoucheShampoing(): ?bool
    {
        return $this->douche_shampoing;
    }

    public function setDoucheShampoing(?bool $douche_shampoing): self
    {
        $this->douche_shampoing = $douche_shampoing;

        return $this;
    }

    public function getToiletteComplete(): ?bool
    {
        return $this->toilette_complete;
    }

    public function setToiletteComplete(?bool $toilette_complete): self
    {
        $this->toilette_complete = $toilette_complete;

        return $this;
    }

    public function getSoinsDeBouche(): ?bool
    {
        return $this->soinsDeBouche;
    }

    public function setSoinsDeBouche(?bool $soinsDeBouche): self
    {
        $this->soinsDeBouche = $soinsDeBouche;

        return $this;
    }

    public function getSoinsEsthetique(): ?bool
    {
        return $this->soinsEsthetique;
    }

    public function setSoinsEsthetique(?bool $soinsEsthetique): self
    {
        $this->soinsEsthetique = $soinsEsthetique;

        return $this;
    }

    public function getHabillage(): ?bool
    {
        return $this->habillage;
    }

    public function setHabillage(?bool $habillage): self
    {
        $this->habillage = $habillage;

        return $this;
    }

    public function getPyjama(): ?bool
    {
        return $this->pyjama;
    }

    public function setPyjama(?bool $pyjama): self
    {
        $this->pyjama = $pyjama;

        return $this;
    }

    public function getOnglesDeMains(): ?bool
    {
        return $this->onglesDeMains;
    }

    public function setOnglesDeMains(?bool $onglesDeMains): self
    {
        $this->onglesDeMains = $onglesDeMains;

        return $this;
    }


    public function getCremeMassage(): ?bool
    {
        return $this->cremeMassage;
    }

    public function setCremeMassage(?bool $cremeMassage): self
    {
        $this->cremeMassage = $cremeMassage;

        return $this;
    }

    public function getRasage(): ?bool
    {
        return $this->rasage;
    }

    public function setRasage(?bool $rasage): self
    {
        $this->rasage = $rasage;

        return $this;
    }

    public function getChangmentDePosition(): ?bool
    {
        return $this->changmentDePosition;
    }

    public function setChangmentDePosition(?bool $changmentDePosition): self
    {
        $this->changmentDePosition = $changmentDePosition;

        return $this;
    }

    public function getRefectionLit(): ?bool
    {
        return $this->refectionLit;
    }

    public function setRefectionLit(?bool $refectionLit): self
    {
        $this->refectionLit = $refectionLit;

        return $this;
    }

    public function getChangementDeDrap(): ?bool
    {
        return $this->changementDeDrap;
    }

    public function setChangementDeDrap(?bool $changementDeDrap): self
    {
        $this->changementDeDrap = $changementDeDrap;

        return $this;
    }

    public function getAspirateur(): ?bool
    {
        return $this->aspirateur;
    }

    public function setAspirateur(?bool $aspirateur): self
    {
        $this->aspirateur = $aspirateur;

        return $this;
    }

    public function getVaisselle(): ?bool
    {
        return $this->vaisselle;
    }

    public function setVaisselle(?bool $vaisselle): self
    {
        $this->vaisselle = $vaisselle;

        return $this;
    }

    public function getLessive(): ?bool
    {
        return $this->lessive;
    }

    public function setLessive(?bool $lessive): self
    {
        $this->lessive = $lessive;

        return $this;
    }

    public function getNettoyageSanitaire(): ?bool
    {
        return $this->nettoyageSanitaire;
    }

    public function setNettoyageSanitaire(?bool $nettoyageSanitaire): self
    {
        $this->nettoyageSanitaire = $nettoyageSanitaire;

        return $this;
    }

    public function getRangementPlacardVetement(): ?bool
    {
        return $this->rangementPlacardVetement;
    }

    public function setRangementPlacardVetement(?bool $rangementPlacardVetement): self
    {
        $this->rangementPlacardVetement = $rangementPlacardVetement;

        return $this;
    }

    public function getRepassage(): ?bool
    {
        return $this->repassage;
    }

    public function setRepassage(?bool $repassage): self
    {
        $this->repassage = $repassage;

        return $this;
    }

    public function getDepoussierageMeubles(): ?bool
    {
        return $this->depoussierageMeubles;
    }

    public function setDepoussierageMeubles(?bool $depoussierageMeubles): self
    {
        $this->depoussierageMeubles = $depoussierageMeubles;

        return $this;
    }

    public function getNettoyageFrigo(): ?bool
    {
        return $this->nettoyageFrigo;
    }

    public function setNettoyageFrigo(?bool $nettoyageFrigo): self
    {
        $this->nettoyageFrigo = $nettoyageFrigo;

        return $this;
    }

    public function getNettoyageMicroOnde(): ?bool
    {
        return $this->nettoyageMicroOnde;
    }

    public function setNettoyageMicroOnde(?bool $nettoyageMicroOnde): self
    {
        $this->nettoyageMicroOnde = $nettoyageMicroOnde;

        return $this;
    }

    public function getDescentePoubelle(): ?bool
    {
        return $this->descentePoubelle;
    }

    public function setDescentePoubelle(?bool $descentePoubelle): self
    {
        $this->descentePoubelle = $descentePoubelle;

        return $this;
    }

    public function getCommunication(): ?bool
    {
        return $this->communication;
    }

    public function setCommunication(?bool $communication): self
    {
        $this->communication = $communication;

        return $this;
    }

    public function getCourses(): ?bool
    {
        return $this->courses;
    }

    public function setCourses(?bool $courses): self
    {
        $this->courses = $courses;

        return $this;
    }

    public function getPreparationPetitDejeunerLendemain(): ?bool
    {
        return $this->preparationPetitDejeunerLendemain;
    }

    public function setPreparationPetitDejeunerLendemain(?bool $preparationPetitDejeunerLendemain): self
    {
        $this->preparationPetitDejeunerLendemain = $preparationPetitDejeunerLendemain;

        return $this;
    }

    public function getPromenade(): ?bool
    {
        return $this->promenade;
    }

    public function setPromenade(?bool $promenade): self
    {
        $this->promenade = $promenade;

        return $this;
    }

    public function getAppreterLeGout(): ?bool
    {
        return $this->appreterLeGout;
    }

    public function setAppreterLeGout(?bool $appreterLeGout): self
    {
        $this->appreterLeGout = $appreterLeGout;

        return $this;
    }

    public function getRepas(): ?bool
    {
        return $this->repas;
    }

    public function setRepas(?bool $repas): self
    {
        $this->repas = $repas;

        return $this;
    }

    public function getPriseMedicaments(): ?bool
    {
        return $this->priseMedicaments;
    }

    public function setPriseMedicaments(?bool $priseMedicaments): self
    {
        $this->priseMedicaments = $priseMedicaments;

        return $this;
    }

    public function getSelles(): ?bool
    {
        return $this->selles;
    }

    public function setSelles(?bool $selles): self
    {
        $this->selles = $selles;

        return $this;
    }

    public function getUrine(): ?bool
    {
        return $this->urine;
    }

    public function setUrine(?bool $urine): self
    {
        $this->urine = $urine;

        return $this;
    }

    public function getEtatCutaneFriction(): ?bool
    {
        return $this->etatCutaneFriction;
    }

    public function setEtatCutaneFriction(?bool $etatCutaneFriction): self
    {
        $this->etatCutaneFriction = $etatCutaneFriction;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getPatient(): ?Patient
    {
        return $this->patient;
    }

    public function setPatient(?Patient $patient): self
    {
        $this->patient = $patient;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Function dateInitialise
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     * User: emayemba
     * Date: 04/11/2020
     */
    public function dateInitialise()
    {
        if(empty($this->createdAt)){
            $this->createdAt=new \DateTime();
        }
    }
}
