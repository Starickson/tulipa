<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201113220924 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE family (id INT AUTO_INCREMENT NOT NULL, family_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE family_user (family_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_F9E4746DC35E566A (family_id), INDEX IDX_F9E4746DA76ED395 (user_id), PRIMARY KEY(family_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fiche (id INT AUTO_INCREMENT NOT NULL, patient_id INT NOT NULL, user_id INT NOT NULL, comment LONGTEXT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_4C13CC786B899279 (patient_id), INDEX IDX_4C13CC78A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE intervention (id INT AUTO_INCREMENT NOT NULL, patient_id INT DEFAULT NULL, user_id INT DEFAULT NULL, douche_shampoing TINYINT(1) DEFAULT NULL, toilette_complete TINYINT(1) DEFAULT NULL, soins_de_bouche TINYINT(1) DEFAULT NULL, soins_esthetique TINYINT(1) DEFAULT NULL, habillage TINYINT(1) DEFAULT NULL, pyjama TINYINT(1) DEFAULT NULL, ongles_de_mains TINYINT(1) DEFAULT NULL, creme_massage TINYINT(1) DEFAULT NULL, rasage TINYINT(1) DEFAULT NULL, changment_de_position TINYINT(1) DEFAULT NULL, refection_lit TINYINT(1) DEFAULT NULL, changement_de_drap TINYINT(1) DEFAULT NULL, aspirateur TINYINT(1) DEFAULT NULL, vaisselle TINYINT(1) DEFAULT NULL, lessive TINYINT(1) DEFAULT NULL, nettoyage_sanitaire TINYINT(1) DEFAULT NULL, rangement_placard_vetement TINYINT(1) DEFAULT NULL, repassage TINYINT(1) DEFAULT NULL, depoussierage_meubles TINYINT(1) DEFAULT NULL, nettoyage_frigo TINYINT(1) DEFAULT NULL, nettoyage_micro_onde TINYINT(1) DEFAULT NULL, descente_poubelle TINYINT(1) DEFAULT NULL, communication TINYINT(1) DEFAULT NULL, courses TINYINT(1) DEFAULT NULL, preparation_petit_dejeuner_lendemain TINYINT(1) DEFAULT NULL, promenade TINYINT(1) DEFAULT NULL, appreter_le_gout TINYINT(1) DEFAULT NULL, repas TINYINT(1) DEFAULT NULL, prise_medicaments TINYINT(1) DEFAULT NULL, selles TINYINT(1) DEFAULT NULL, urine TINYINT(1) DEFAULT NULL, etat_cutane_friction TINYINT(1) DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_D11814AB6B899279 (patient_id), INDEX IDX_D11814ABA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE patient (id INT AUTO_INCREMENT NOT NULL, family_id INT DEFAULT NULL, gender VARCHAR(1) DEFAULT NULL, last_name VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, phone VARCHAR(15) DEFAULT NULL, adress VARCHAR(255) NOT NULL, floor VARCHAR(5) DEFAULT NULL, cp VARCHAR(5) NOT NULL, avatar VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, town VARCHAR(255) NOT NULL, code VARCHAR(10) DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, INDEX IDX_1ADAD7EBC35E566A (family_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, password VARCHAR(255) NOT NULL, picture VARCHAR(255) DEFAULT NULL, is_validate TINYINT(1) NOT NULL, phone VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, last_name VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, roles VARCHAR(255) NOT NULL, informations LONGTEXT DEFAULT NULL, reset_token VARCHAR(255) DEFAULT NULL, code VARCHAR(10) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE family_user ADD CONSTRAINT FK_F9E4746DC35E566A FOREIGN KEY (family_id) REFERENCES family (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE family_user ADD CONSTRAINT FK_F9E4746DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fiche ADD CONSTRAINT FK_4C13CC786B899279 FOREIGN KEY (patient_id) REFERENCES patient (id)');
        $this->addSql('ALTER TABLE fiche ADD CONSTRAINT FK_4C13CC78A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE intervention ADD CONSTRAINT FK_D11814AB6B899279 FOREIGN KEY (patient_id) REFERENCES patient (id)');
        $this->addSql('ALTER TABLE intervention ADD CONSTRAINT FK_D11814ABA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE patient ADD CONSTRAINT FK_1ADAD7EBC35E566A FOREIGN KEY (family_id) REFERENCES family (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE family_user DROP FOREIGN KEY FK_F9E4746DC35E566A');
        $this->addSql('ALTER TABLE patient DROP FOREIGN KEY FK_1ADAD7EBC35E566A');
        $this->addSql('ALTER TABLE fiche DROP FOREIGN KEY FK_4C13CC786B899279');
        $this->addSql('ALTER TABLE intervention DROP FOREIGN KEY FK_D11814AB6B899279');
        $this->addSql('ALTER TABLE family_user DROP FOREIGN KEY FK_F9E4746DA76ED395');
        $this->addSql('ALTER TABLE fiche DROP FOREIGN KEY FK_4C13CC78A76ED395');
        $this->addSql('ALTER TABLE intervention DROP FOREIGN KEY FK_D11814ABA76ED395');
        $this->addSql('DROP TABLE family');
        $this->addSql('DROP TABLE family_user');
        $this->addSql('DROP TABLE fiche');
        $this->addSql('DROP TABLE intervention');
        $this->addSql('DROP TABLE patient');
        $this->addSql('DROP TABLE user');
    }
}
